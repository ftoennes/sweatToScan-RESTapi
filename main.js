const express = require('express'),
basicAuth = require('express-basic-auth'),
bodyParser = require('body-parser'),
formidable = require('formidable'),
redis = require('redis'),
fs = require('fs'),
port = 3000, //listen at port...

app = express(),
imgBasePath = '/www/media/img/',
imgFileType = '.png',
redisClient = redis.createClient() //Standard is host: 127.0.0.1, port: 6379

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next)=>{
  print('request at: '+req.url+' '+req.method);
  next();
})



var access = fs.createWriteStream('/www/log/node_access.log', {flags: 'a'});
process.stdout.write = process.stderr.write = access.write.bind(access);



process.on('uncaughtException', function(err) {
  console.error((err && err.stack) ? err.stack : err);
});

redisClient.on('connect', ()=>{
  print('Redis connected')
})
redisClient.on('error', (err)=>{
    print('Redis error ' + err)
})

app.get('/api/all_exercises', (request, response)=>{
  var body = {}
  //get All DB keys
  getAllValuesFromDB((keys, exercises)=>{
    for(index in exercises){
      var exercise = exercises[index]

      body[keys[index]] = exercise
    }

    response.setHeader('Cache-Control', 'public, max-age=3600')
    response.status(200).send(body)
  })
})

app.get('/api/all_exercises_v2', (request, response)=>{
  var body = []
  //get All DB keys
  getAllValuesFromDB((keys, exercises)=>{
    for(index in exercises){
      var exercise = {}
      //exercises[index]
      //body[keys[index]] = exercise
      var values = exercises[index].split(';')

      exercise["id"] = keys[index]
      exercise["name"] = values[0].trim()
      exercise["muskel"] = values[1].trim()
      exercise["bild-id"] = values[2].trim()
      exercise["geraete-id"] = values[3].trim()
      exercise["desc"] = values[4].trim()

      body.push(exercise)
    }

    response.setHeader('Cache-Control', 'public, max-age=3600')
    response.status(200).send(body)
  })
})



//----------------Reading from DB  ----------------------------------------------

app.get('/api/exercise', (request, response)=>{
  //validate query
  var key = (request.query.id+"").toUpperCase()
  if(! key.match(/[A-Z][0-9]{2}/)){
    response.status(404).end()
    print('ID entspricht nicht Regular Expression');
    return;
  }


  //get data from db
  redisClient.get(key, (error, result)=>{
    if(error){
      print(error)
      response.status(500).end()
      return
    }


    if(result){
      //verify result before sending ok

      //tell NGINX to cache response
      response.setHeader('Cache-Control', 'public, max-age=3600')
      response.status(200).send(result)
      print('serve request for '+key)

    }else{
      response.status(404).end()
      print('nothing found for '+key)
    }
  })
})






//DELETE From DB and Image File from


//only Authorized Users can Post new Data to DB
//check login
app.delete("/api/exercise", basicAuth({
    users: { 'scantosweat': 'geh3im'}
}))

app.delete('/api/exercise', (request, response)=>{
  var key = (request.query.id+"").toUpperCase()

  //delete image if exists
  redisClient.mget(key,(err, result)=>{
    var filename = result[0].split(/;|; /g)[2];
    if (filename === '') return;

    var filepath = imgBasePath + filename + imgFileType
    fs.unlink(filepath, (err)=>{
      if (err) console.log(err);
    })

  })

  redisClient.del(key, (err, result)=>{
    console.log('deleted')
  })



  //if delete successful
  response.status(200).send()
})














//------------------------writing do DB ------------------------------------------

//only Authorized Users can Post new Data to DB
//check login
app.post("/api/exercise", basicAuth({
    users: { 'scantosweat': 'geh3im'}
}))

app.post('/api/submit_exercise', (request, response)=>{
  var key, value,

  img_filename = '',
  upload_finished
  form = new formidable.IncomingForm()
  console.log(form);
  //TODO: check for XSS, add padding to data

  form.on('fileBegin', function (name, file){
    if (file.name === '') return;
    img_filename = getAvailableFilename()
    file.path = imgBasePath + img_filename + imgFileType;
  })

  form.on('file', function (name, file){
    print('Uploaded Image: ' + file.name);

  })

  form.parse(request, function (err, fields, files) {
    //console.log(err);
    //console.log(fields);

    key = (fields.id).toUpperCase()


      // Felder validieren
      fields.exercise_name = (fields.exercise_name+"")
        .replace(/ {1,}/, ' ')
        .match(/[\wäöü][\w .,()\/&äöüß-]{1,48}[\wäöü.,]/i)

      fields.muscle = (fields.muscle+"")
        .replace(/ {1,}/, ' ')
        .match(/[\wäöü][\w .,()\/&äöüß-]{1,48}[\wäöü.,]/i)

      fields.machine = (fields.machine+"")
        .replace(/ {1,}/, ' ')
        .match(/[\wäöü][\w .,()\/&äöüß-]{1,48}[\wäöü.,]/i)

      fields.description = (fields.description+"")
        .replace(/ {1,}/, ' ')
        .match(/[\wäöü][\w .,()\/&äöüß-]{1,4000}[\wäöü.,]/i)





      value = fields.exercise_name+';'+fields.muscle+';'+img_filename+';'+fields.machine+';'+fields.description+';';

      redisClient.set(key, value, function(err, res){
        //console.log(err);
        print("DB response after Insert: "+res);
      })
  });

  response.setHeader('Cache-Control', 'no-cache');
  response.status(200).send()
})








//check login
app.get("/api/add_exercise", basicAuth({
    users: { 'scantosweat': 'geh3im' },
    challenge: true,
    realm: 'scanToSweat'
}))

app.get("/api/add_exercise", (request, response)=>{
  var html = fs.readFileSync('html/add_exercise.html', 'utf8'),
  row_template = fs.readFileSync('html/exercise_tr_template.html', 'utf8'),
  option_template = fs.readFileSync('html/option_template.html', 'utf8'),
  table = "",
  responded = false

  //get All DB keys

  getAllValuesFromDB((keys, exercises)=>{
    //  console.log(keys);
    for(index in exercises){
      var exercise = exercises[index]
      exercise = exercise.split(/;|; /g)
      exercise.pop()
      exercise.push(keys[index])
      exercise = exercise.reverse()//.push(exercise[0])
      exercise.push(keys[index])

      //add values & id to html table
      table += row_template.replace(/@insert:[a-zA-Z_0-9]{1,};/g, ()=>{
        var cell = exercise.pop()
        return (cell ? cell : '---' )
      })
    }

    html = html.replace('@insert:DB_Content;', table)

    response.setHeader('Cache-Control', 'no-cache');
    response.status(200).send(html)
    responded = true
  })

  //set Timeout for compiling site to 500ms, if timeout, then send error 500
  setTimeout(()=>{
    if(! responded)
      response.status(500).end('Connection to Database timed out, please try again later')
  }, 500)
})






// catch requests to unavailable URIs and return 404
app.get('*', (request, response)=>{
    response.status(404).end()
})
app.post('*', (request, response)=>{
    response.status(404).end()
})


app.listen(port, ()=>{
  console.log('\n\nREST Server started, listening on port 3000');
})


//utilities
function print(text){
  var d = new Date,
  time = d.toISOString()
  console.log(time+'  '+text)
}

function getAvailableFilename(){
  var img_filename = ""

  do{
    var possibleChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-"
    var generatedFileName = ""

    //Generate 4 letter random words
    for(var i = 0; i < 4; i++){
      generatedFileName += possibleChars.charAt(Math.floor(Math.random()*possibleChars.length))
    }

    if (!fs.existsSync(imgBasePath+generatedFileName+imgFileType)) {
      img_filename = generatedFileName
    }
    //Solange kein Nicht existierender Name gefunden wurde wiederholen
  } while (!img_filename)

  return img_filename
}

function getAllValuesFromDB(callback){
  redisClient.dbsize((err, keyCount)=>{
    print('DB has: ' + keyCount + ' entries')
    redisClient.scan(0, 'COUNT', keyCount, (err, result)=>{
      var keys = result[1]
      if(err){
        response.status(500).end();
        return print(err)
      }

      //get all DB values
      redisClient.mget(keys,(err, exercises)=>{
        if(err){
          response.status(500).end();
          return print(err)
        }

        callback(keys, exercises)
      })
    })
  })
}
