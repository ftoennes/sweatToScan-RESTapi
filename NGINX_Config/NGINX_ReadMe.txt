Server Ordnerstruktur:

/
  www/media/img/      -> hier liegen die Bilddateien
  var/
    cache/nginx/      -> hier ist der Cache
    log/nginx/
      error.log       -> Fehler Logs
      access.log      -> Alle Requests Logs
  etc/nginx/
    nginx.conf        -> Hier muss die Config-Datei gespeichert werden



Im nginx.conf müssen ggf die Anzahl der Worker Prozesse & Max Connection angepasst werden.
Außerdem müssen SSL Zertifikat & Schlüssel generiert werden & der Pfad in der nginx.conf eingefügt werden
